# Push Notifications app for Alliance Auth

This is an push notification plugin for [Alliance Auth](https://gitlab.com/allianceauth/allianceauth) (AA) that can be used to get realtime updates from your AA server.

![License](https://img.shields.io/badge/license-GPL-green) ![python](https://img.shields.io/badge/python-3.6-informational) ![django](https://img.shields.io/badge/django-3.1-informational)

## Features

- Send push notifications to your browser or phone!

# Installation

## Step 1 - Install Apps

Add the following line to your `urls.py`:

```python
urlpatterns = [
    ...
    url(r'^webpush/', include('webpush.urls'))  # Add this line
]
```

Also add the following apps to your `local.py`:

- `webpush`
- `pushnotifications`

## Step 2 - Generate VAPID Keys

If you would like to send notification to Google Chrome Users, you need to add a ``WEBPUSH_SETTINGS`` entry with the **Vapid Credentials** Like following:
```python
WEBPUSH_SETTINGS = {
    "VAPID_PUBLIC_KEY": "Vapid Public Key",
    "VAPID_PRIVATE_KEY":"Vapid Private Key",
    "VAPID_ADMIN_EMAIL": "admin@example.com"
}
```
**Replace ``"Vapid Public Key"`` and ``"Vapid Private Key"`` with your Vapid Keys. Also replace ``admin@example.com`` with your email so that the push server of browser can reach to you if anything goes wrong.**

> **To know how to obtain Vapid Keys please see this [`py_vapid`](https://github.com/web-push-libs/vapid/tree/master/python) and [Google Developer Documentation](https://developers.google.com/web/fundamentals/push-notifications/subscribing-a-user#how_to_create_application_server_keys). You can obtain one easily from [web-push-codelab.glitch.me](https://web-push-codelab.glitch.me/). ``Application Server Keys`` and ``Vapid Keys`` both are same.**
