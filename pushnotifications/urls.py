from django.conf.urls import include, url
from django.urls import path

from . import views

app_name = "pushnotifications"

urlpatterns = [
    path("", views.index, name="index"),
    url(r"^webpush/", include("webpush.urls")),
]
