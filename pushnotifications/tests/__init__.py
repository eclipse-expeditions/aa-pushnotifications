from typing import Tuple

from django.contrib.auth.models import User

from allianceauth.authentication.models import CharacterOwnership
from allianceauth.eveonline.models import EveCharacter
from allianceauth.tests.auth_utils import AuthUtils
from esi.models import Token

from .utils import add_new_token


def create_user_from_evecharacter(character_id: int) -> Tuple[User, CharacterOwnership]:
    auth_character = EveCharacter.objects.get(character_id=character_id)
    user = AuthUtils.create_user(auth_character.character_name)

    character_ownership = add_character_to_user(
        user,
        auth_character,
        is_main=True,
        scopes=["publicData"],
    )
    return user, character_ownership


def add_character_to_user(
    user: User,
    character: EveCharacter,
    is_main: bool = False,
    scopes: list = None,
) -> CharacterOwnership:
    if not scopes:
        scopes = "publicData"

    token = add_new_token(user, character, scopes)
    token.save()
    if is_main:
        user.profile.main_character = character
        user.profile.save()
        user.save()

    return CharacterOwnership.objects.get(user=user, character=character)


def scope_names_set(token: Token) -> set:
    return set(token.scopes.values_list("name", flat=True))
