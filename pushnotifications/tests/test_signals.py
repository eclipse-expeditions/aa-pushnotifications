from unittest.mock import patch

from allianceauth.notifications import notify

from . import create_user_from_evecharacter
from .testdata.load_entities import load_entities
from .utils import NoSocketsTestCase

SIGNALS_PATH = "pushnotifications.signals"


@patch(SIGNALS_PATH + ".send_user_notification")
class TestSignals(NoSocketsTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        load_entities()
        cls.user, _ = create_user_from_evecharacter(1101)

    def test_send_push_notification_on_notify(self, mock_webpush):

        notify(self.user, "Test Message", "Test Body")
        self.assertTrue(mock_webpush.called)
