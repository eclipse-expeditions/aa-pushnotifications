from django.utils.translation import ugettext_lazy as _

from allianceauth import hooks
from allianceauth.services.hooks import MenuItemHook, UrlHook

from . import urls


class PushNotificationsMenuItem(MenuItemHook):
    def __init__(self):
        MenuItemHook.__init__(
            self,
            _("Push Notifications"),
            "fas fa-envelope-open-text fa-fw",
            "pushnotifications:index",
            navactive=["pushnotifications:index"],
        )

    def render(self, request):
        return MenuItemHook.render(self, request)


@hooks.register("menu_item_hook")
def register_menu():
    return PushNotificationsMenuItem()


@hooks.register("url_hook")
def register_urls():
    return UrlHook(urls, "pushnotifications", r"^pushnotifications/")
