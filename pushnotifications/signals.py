from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from webpush import send_user_notification

from allianceauth.notifications.models import Notification


@receiver(post_save, sender=Notification)
def send_push_notification(sender, instance: Notification, created: bool, **kwargs):
    if created:
        payload = {
            "head": settings.SITE_NAME + " :: " + instance.title,
            "body": instance.message,
        }
        try:
            send_user_notification(user=instance.user, payload=payload, ttl=1000)
        except Exception:
            pass
