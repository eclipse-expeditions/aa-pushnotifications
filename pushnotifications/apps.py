from django.apps import AppConfig


class PushNotificationsConfig(AppConfig):
    name = "pushnotifications"
    label = "pushnotifications"
    verbose_name = "Push Notifications"

    def ready(self):
        from . import signals  # noqa: F401
